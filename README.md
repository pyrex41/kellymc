# KellyMC

Clone repository, then run setup. Run quick sim using `script.jl':

``` julia
╭─░▒▓ ~/.julia/dev/KellyMC │ main !1 ?1 ───────── ✔ │ 1m 50s │ 17:18:58 ▓▒░
╰─ julia --project -i setup.jl
    Updating registry at `~/.julia/registries/General.toml`
  No Changes to `~/.julia/dev/KellyMC/Project.toml`
  No Changes to `~/.julia/dev/KellyMC/Manifest.toml`
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.7.2 (2022-02-06)
 _/ |\__'_|_|_|\__'_|  |  HEAD/bf53498635 (fork: 461 commits, 426 days)
|__/                   |

julia> include("script.jl")
Simulation parameters
price_init = 100.0
μ = 0.0
strike_price = 100.0
σ = 0.5
T = 1
n_mc_paths = 10000000
BS price of call:
option_price = 19.741265136584744
--------------------

  5.690149 seconds (93.35 M allocations: 22.298 GiB, 50.66% gc time, 7.88% compilation time)

Descriptive stats for long option payofs:
mean(v) = 19.732011184931856
quantile(v, [0.1, 0.5, 0.9]) = [0.0, 0.0, 67.48650538702661]
std(v) = 39.67591181851909
var(v) = 1574.1779786309025
skewness(v) = 3.2923569424411077

-------------------------------------
```

Can also run script from command line:

``` julia
╭─░▒▓ ~/.julia/dev/KellyMC │ main !1 ?1 ───────── ✔ │ 1m 34s │ 17:20:41 ▓▒░
╰─ julia --project script.jl
Simulation parameters
price_init = 100.0
μ = 0.0
strike_price = 100.0
σ = 0.5
T = 1
n_mc_paths = 10000000
BS price of call:
option_price = 19.741265136584744
--------------------

  5.425443 seconds (93.35 M allocations: 22.084 GiB, 48.65% gc time, 8.54% compilation time)

Descriptive stats for long option payofs:
mean(v) = 19.742254553834304
quantile(v, [0.1, 0.5, 0.9]) = [0.0, 0.0, 67.57605372849463]
std(v) = 39.67616208261065
var(v) = 1574.197837605591
skewness(v) = 3.2953858652607804

-------------------------------------
```
