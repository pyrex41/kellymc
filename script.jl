using KellyMC, Transducers, Statistics, StatsBase


# functions assume vol, drift are anualized but T is given in days, assuming 252 trading days
const price_init = 100.0
const μ = 0.0
const strike_price = 100.0
const σ = 0.5
const T = 1 # 1 year
const n_mc_paths = 10^6

println("Simulation parameters")
@show price_init
@show μ
@show strike_price
@show σ
@show T
@show n_mc_paths

option_price = bs_call(price_init, strike_price, T, σ)
println("BS price of call:")
@show option_price
println("--------------------")
println()

cost = .9 * option_price
call_payoff(x::Real) = max(x - strike_price, 0)

@time long_call_payoffs =
    mc_result(price_init, μ, σ; n=252*T, npaths=n_mc_paths, func=call_payoff)

function show_results(v::Vector{Float64})
    @show mean(v)
    @show quantile(v, [.1,.5,.9])
    @show std(v)
    @show var(v)
    @show skewness(v)
    println()
end

println()
println("Descriptive stats for long option payoff")
show_results(long_call_payoffs)
println("-------------------------------------")

cost = .9 * option_price

call_return(x::Real) = (call_payoff(x) - cost) / cost

@time long_call_returns =
    mc_result(price_init, μ, σ; n=252*T, npaths=n_mc_paths, func=call_return)

println()
println("Return Stats using premium cost at 90% of BS price:")
show_results(long_call_returns)
println("-------------------------------------")


println()
println("Kelly fraction of return stream using only first 2 moments:")
@show kelly(long_call_returns, 2)
println("---------------------------------")
println("Kelly fraction of return stream using 4 moments:")
@show kelly(long_call_returns, 4)
println("--------------------------------")
