function GBlackScholes(side, S::Real, X::Real, T::Real, r::Real, sigma::Real, q::Real=0.0)
    s_side = lowercase(string(side))
    @assert s_side in ["put", "call"]
    phi(x) = 1/2 * (1 + erf(x / sqrt(2)))
    b = r - q
    d1 = (log(S/X) + (b + sigma^2 / 2) * T) / (sigma * sqrt(T))
    d2 = d1 - sigma * sqrt(T)
    ii = s_side == "call" ? 1 : -1
    ii * S * exp((b - r) * T) * phi(ii * d1) - ii * X * exp(-r * T) * phi(ii * d2)
end

function iter(v1, v2, p1, p2, p, vf, pf, epsilon = 1e-10)
    vi = vf(v1, v2, p1, p2)
    pv = pf(vi)
    ee = abs(p - pv) < epsilon
    if pv < p
        vv1 = vi
        pp1 = pv
        vv2, pp2 = v2, p2
        return vv1, vv2, pp1, pp2, ee, true
    else
        vv1, pp1 = v1, p1
        vv2 = vi
        pp2 = pv
        return vv1, vv2, pp1, pp2, ee, false
    end
end

function delta(;μ::Real=0.0, S::Real, X::Real, T::Real, r::Real=0.01, σ::Real)
    d1 = (log(S/X) + (r + μ + σ^2/2)*T)  / (σ * sqrt(T))
    ϕ(d1)
end

# μ = -q
function gamma(;μ::Real=0.0, S::Real, X::Real, T::Real, r::Real=0.01, σ::Real)
    d1 = (log(S/X) + (r + μ + σ^2/2)*T)  / (σ * sqrt(T))
    ϕ(d1)*exp(μ*T) / (S*σ*sqrt(T))
end

function vega(;μ::Real=0.0, S::Real, X::Real, T::Real, r::Real=0.01, σ::Real)
    d1 = (log(S/X) + (r + μ + σ^2/2)*T)  / (σ * sqrt(T))
    S*exp(μ*T) *  ϕ(d1) * sqrt(T)
end


function ImpliedVolatility(side, p::Real,  S::Real, X::Real, T::Real, r::Real, q::Real=0.0, epsilon::Float64 = 1e-8)
    sside = lowercase(string(side))
    @assert sside in ["put", "call"]
    #=
    if sside == "put"
        @assert X <= S
    else
        @assert X >= S
    end
    =#
    v1_init = 0.005
    v2_init = 4

    vi_func(v1, v2, p1, p2) = v1 + (p - p1) * (v2-v1) / (p2 - p1)
    price_func(v) = GBlackScholes(sside, S, X,  T, r, v, q)

    p1_init = price_func(v1_init)
    p2_init = price_func(v2_init)

    v1, v2, p1, p2, _, low = iter(0.005, 4, p1_init, p2_init, p, vi_func, price_func, epsilon)

    i = 0
    while i < 1000
        v1,v2,p1,p2,retBool,low = iter(v1,v2,p1,p2,p, vi_func, price_func)
        if retBool
            return low ? v1 : v2
        end
        i += 1
    end
    low ? v1 : v2
end
