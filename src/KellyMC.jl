module KellyMC

export mc_paths, bs_call, mc_endpoints, mc_result, kelly, raw_moment

import Base.Threads.@threads

include("BlackScholes.jl")

bs_call(S, K, T, σ, r=0, q=0) = GBlackScholes("call", S, K, T, r, σ, q)

using Distributions, Transducers, SpecialFunctions, Polynomials
# Write your package code here.

function random_return(μ::Real, σ::Real, n::Integer)
    d = Normal(μ, σ)
    rand(d, n)
end

function price_res(price_init::Real, μ::Real, σ::Real; n::Integer=100, annualized=true)
    μ = annualized ? μ / 252 : μ
    σ = annualized ? σ / sqrt(252) : σ
    rets = random_return(μ, σ, n)
    foldl(rets, init=price_init) do p0, r1
        p0 * (1 + r1)
    end
end

function mc_result(price::Real, μ::Real, σ::Real; n::Integer=100, npaths::Integer=10000, annualized=true, func::Function = x -> x)

    out = zeros(npaths)
    @threads for i=1:npaths
        s = price_res(price, μ, σ, n=n, annualized=annualized)
        out[i] = func(s)
    end
    out
end

function kelly(vec::Vector{Float64}, k::Int=4)
    k = mod(k, 2) + k # must be an even number
    k = max(k, 2) # can't be negative or 0
    moms = zeros(k)
    @threads for i=1:k
        moms[i] = raw_moment(vec, i)
    end
    kelly_moms(moms)
end

function kelly_moms(moms::Vector{Float64})
    coef_vec = zeros(length(moms))
    for (i, m) in enumerate(moms)
        coef_vec[i] = (-1)^(i-1) * m
    end
    sol = roots(Polynomial(coef_vec))
    out = filter(isreal, sol) |> minimum
    typeof(out) == Float64  ? out : out.re
end

function raw_moment(vec, k::Int)
    n = length(vec)
    1/n * (vec |> Map(x->x^k) |> sum)
end


end
